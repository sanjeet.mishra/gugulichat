const express = require('express');
const app = express();
const connection = require('./DB/db');
const cors = require('cors');
const userRoutes = require('./Routes/userRoutes');


app.use(cors());
app.use(express.json());
connection();

app.use('/auth', userRoutes);

app.listen((8000), () => {
    console.log(`Server is running on 8000`);
});

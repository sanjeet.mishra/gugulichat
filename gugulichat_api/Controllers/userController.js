const User = require('../Models/userModel');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

// This is for register
exports.register = async (req, res) => {
    console.log(req.body);

    try {
        const isEmailExist = await User.findOne({ email: req.body.email });

        if (isEmailExist) {
            res.status(400).json({ message: "Email already exists" })
        }

        const hashedPassword = await bcrypt.hash(req.body.password, 11);

        const user = new User({
            fullname: req.body.fullname,
            email: req.body.email,
            password: hashedPassword,
        });

        const token = jwt.sign({userId: user._id, name: user.fullname}, process.env.SECRETKEY);

        await user.save();

        res.send(token);
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: "Error occurred while registering" });
    }
};

// This is for login
exports.login = async (req, res) => {
    console.log(req.body);

    try {
        const user = await User.findOne({ email: req.body.email });

        if (!user) {
            return res.status(400).json({ message: "User Not Found" });
        };

        const passwordMatch = await bcrypt.compare(req.body.password, user.password);

        if (passwordMatch) {
            const token = jwt.sign({ userId: user._id, name: user.fullname }, process.env.SECRETKEY);
            return res.send(token);
        } else {
            return res.status(400).json({ message: "Invalid Email or Password" });
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: "Error occurred while logging in" });
    }
}
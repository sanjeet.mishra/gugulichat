import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import jwt_decode from 'jwt-decode';

const Register = () => {
    const navigate = useNavigate();

    const [data, setData] = useState({
      fullname: '',
      email: '',
      password: '',
    });
  
    const changeHandler = (e) => {
      setData({ ...data, [e.target.name]: e.target.value });
    };
  
    const submitHandler = async(e) => {
        e.preventDefault();
        try {
            const response = await axios.post('http://localhost:8000/auth/register', data);
            console.log(response.data);
            if (response) {
                const decodedToken = jwt_decode(response.data);
                localStorage.setItem("data", JSON.stringify(decodedToken));
                navigate('/');
                window.location.reload();
            };
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <>
            <div className='container-fluid'>
                <div className='row justify-content-center'>
                    <h2 className='py-4'>Register</h2>
                    <div className='col-lg-4'>
                        <form onSubmit={submitHandler}>
                            <div className="form-outline mb-4">
                                <label className="form-label" htmlFor="form2Example1">Full Name</label>
                                <input type="text" id="fullname" name="fullname" className="form-control" onChange={changeHandler} value={data.fullname} />
                            </div>

                            <div className="form-outline mb-4">
                                <label className="form-label" htmlFor="email">Email address</label>
                                <input type="email" id="email" name="email" className="form-control" onChange={changeHandler} value={data.email} />
                            </div>

                            <div className="form-outline mb-4">
                                <label className="form-label" htmlFor="password">Password</label>
                                <input type="password" name="password" className="form-control" onChange={changeHandler} value={data.password} />
                            </div>

                            <button type="submit" className="btn btn-primary btn-block mb-4">Register</button>

                            <div className="text-center">
                                <p>Already a member? <Link to='/login'>Login</Link></p>
                                <button type="button" className="btn btn-link btn-floating mx-1">
                                    <i className="fab fa-facebook-f"></i>
                                </button>

                                <button type="button" className="btn btn-link btn-floating mx-1">
                                    <i className="fab fa-google"></i>
                                </button>

                                <button type="button" className="btn btn-link btn-floating mx-1">
                                    <i className="fab fa-twitter"></i>
                                </button>

                                <button type="button" className="btn btn-link btn-floating mx-1">
                                    <i className="fab fa-github"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Register
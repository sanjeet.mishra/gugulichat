import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

const Navigation = () => {
    const [userData, setUserData] = useState("");

    useEffect(() => {
        setUserData(JSON.parse(localStorage.getItem('data')));
    }, [])

    const logoutHandler = () => {
        localStorage.clear();
        window.location.reload();
    }

    return (
        <>
            <div className="navdiv">
                <nav className="navbar navbar-expand-lg">
                    <div className="container-fluid">
                        <Link className="navbar-brand fw-bold px-3 py-2" to="/">Guguli Chat</Link>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav ms-auto mb-2 mb-lg-0 px-4">
                                <li className="nav-item fw-bold px-3 py-2">
                                    <Link className="nav-link" aria-current="page" to="">Home</Link>
                                </li>
                                <li className="nav-item fw-bold px-3 py-2">
                                    <Link className="nav-link" aria-current="page" to="/chat">Chat</Link>
                                </li>
                                <li className="nav-item fw-bold px-3 py-2">
                                    <Link className="nav-link" aria-current="page" to="/about">About</Link>
                                </li>
                                <li className="nav-item fw-bold px-3 py-2">
                                    <Link className="nav-link" aria-current="page" to="/contact">Contact</Link>
                                </li>
                                <li className="nav-item px-3 py-2">

                                    {(userData == undefined) ? (<Link to="/login">
                                        <button type="button" className="btn btn-primary fw-bold px-4">Log In</button>
                                    </Link>
                                    ) : (<button type="button" className="btn btn-primary fw-bold px-4" onClick={logoutHandler}>{userData.name}</button>)}

                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </>
    )
}

export default Navigation